using RPG.Core.Interfaces;
using RPG.Core.Utils;

using UnityEngine;

namespace RPG.Combat {
	public class Health : MonoBehaviour {
		[SerializeField] float health;
		[SerializeField] RectTransform healthBar;

		private CanvasGroup canvasGroup;

		private float _remaningHealth = 100f;
		private bool _isDead = false;

		public float Remaining { get { return _remaningHealth; } }
		public float Total { get { return health; } }
		public bool IsDead { get { return _isDead; } }

		void Awake() {
			_remaningHealth = health;
			canvasGroup = healthBar.parent.GetComponent<CanvasGroup>();

			canvasGroup.alpha = 0;
		}

		void Update() {
			if (_isDead) {
				HideHealthBar(TransitionSpeed.Instant);
			}
		}

		public void Decrease(float amount) {
			_remaningHealth = Mathf.Max(_remaningHealth - amount, 0);
			_isDead = _remaningHealth == 0;
		}

		public void Increase(float amount) {
			_remaningHealth = Mathf.Max(_remaningHealth + amount, 100);
		}

		public void HideHealthBar(TransitionSpeed speed) {
			if (canvasGroup.alpha == 0) { return; }
			if (speed == TransitionSpeed.Instant) {
				canvasGroup.alpha = 0;
			}

			if (canvasGroup.alpha > 0) {
				canvasGroup.alpha -= Time.deltaTime * (int) speed;
			}
		}

		public void ShowHealthBar(TransitionSpeed transitionSpeed) {
			if (transitionSpeed == TransitionSpeed.Instant) { canvasGroup.alpha = 100; }

			CalculateHealthBar();

			if (canvasGroup.alpha < 100) {
				canvasGroup.alpha += Time.deltaTime * (int) transitionSpeed;
			}
		}

		public void Regenerate(TransitionSpeed speed) {
			float healthPercentage = MathUtils.Percentage(_remaningHealth, health);

			if (speed == TransitionSpeed.None) { return; }
			if (healthPercentage < 100 && !_isDead) {
				_remaningHealth += (float) 0.4 * Time.deltaTime * (int) speed;
			}
		}

		private void CalculateHealthBar() {
			float healthPercentage = MathUtils.Percentage(_remaningHealth, health);

			healthBar.sizeDelta = new Vector2(healthPercentage, healthBar.sizeDelta.y);
		}
	}
}