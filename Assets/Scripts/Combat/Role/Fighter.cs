﻿using RPG.Combat.Interfaces;
using RPG.Combat.Shared;

using UnityEngine;

namespace RPG.Combat.Role {
	public class Fighter : CombatCharacterRole, ICombatCharacterRoleActions {
		// animation event
		void Hit() {
			if (combatTarget == null) { return; }
			combatTarget.TakeDamage(weapon.Damage);
		}

		public void Attack(GameObject gameObject) {
			scheduler.StartAction(this);
			combatTarget = gameObject.GetComponent<CombatTarget>();
		}
	}
}