namespace RPG.Combat.Interfaces {
	public enum WeaponType {
		Unarmed,
		TwoHandedSword,
		Staff,
	}
}