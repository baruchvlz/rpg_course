using RPG.Core.Interfaces;
using RPG.Combat.Weapons;

using UnityEngine;

namespace RPG.Combat.Interfaces {
	public interface ICombatCharacterRoleActions : IAction {
		void Attack(GameObject gameObject);
		bool CanAttack(GameObject gameObject);
		bool IsInWeaponRange();
	}
}