using UnityEngine;

namespace RPG.Combat.Weapons {
	public class Weapon : MonoBehaviour {
		[SerializeField] float weaponRange;
		[SerializeField] float weaponDamage;
		[SerializeField] float timeBetweenAttacks;

		public float Damage { get { return weaponDamage; } }
		public float Range { get { return weaponRange; } }
		public float TimeBetweenAttacks { get { return timeBetweenAttacks; } }
	}
}