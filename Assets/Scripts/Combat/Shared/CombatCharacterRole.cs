using RPG.Combat.Interfaces;
using RPG.Combat.Weapons;
using RPG.Core;
using RPG.Movement;

using UnityEngine;

namespace RPG.Combat.Shared {
	/// <summary>
	/// CombatCharacterRole contains the basic attack and movement interactions for combat
	/// <summary>
	[RequireComponent(typeof(Mover))]
	[RequireComponent(typeof(Weapon))]
	[RequireComponent(typeof(Scheduler))]
	[RequireComponent(typeof(Animator))]
	public class CombatCharacterRole : MonoBehaviour {
		[SerializeField] WeaponType weaponType = WeaponType.Unarmed;

		internal Mover mover;
		internal Weapon weapon;
		internal Animator animator;
		internal Scheduler scheduler;
		internal CombatTarget combatTarget;

		internal float timeSinceLastAttack = 0;
		// avoid waiting for time to attack before doing first attack
		internal bool hasStartedAttacking = false;

		void Awake() {
			mover = GetComponent<Mover>();
			animator = GetComponent<Animator>();
			scheduler = GetComponent<Scheduler>();

			weapon = GetWeapon(weaponType);
		}

		void Update() {
			// no target, no need to manage attack
			if (combatTarget == null) { return; }

			ControlMovementAndActions();
		}

		public bool CanAttack(GameObject gameObject) => gameObject != null && !gameObject.GetComponent<Health>().IsDead;
		public bool IsInWeaponRange() => combatTarget && DistanceToCombatTarget() <= weapon.Range;

		public void Cancel() {
			animator.SetTrigger("CancelAttack");
			hasStartedAttacking = false;
			timeSinceLastAttack = 0;
			combatTarget = null;
			animator.ResetTrigger("CancelAttack");
		}

		// TODO: refactor this so it's controlled by the character and not the role.
		internal void ControlMovementAndActions() {
			// reset target if its dead
			if (combatTarget && combatTarget.HitPoints.IsDead) { combatTarget = null; return; }
			// move to target on mouse point click
			if (combatTarget != null && !IsInWeaponRange()) { mover.MoveTo(combatTarget.transform.position); }
			// attack target once in range
			if (IsInWeaponRange()) { AttackAction(); }
		}

		internal void AttackAction() {
			mover.Cancel();
			timeSinceLastAttack += Time.deltaTime;
			transform.LookAt(combatTarget.transform.position);

			if (!hasStartedAttacking || timeSinceLastAttack > weapon.TimeBetweenAttacks) {
				hasStartedAttacking = true;
				animator.SetTrigger("Attack");
				timeSinceLastAttack = 0;
			}
		}

		private Weapon GetWeapon(WeaponType weaponType) {
			switch (weaponType) {
				case WeaponType.Unarmed:
					return GetComponent<Unarmed>();
				case WeaponType.TwoHandedSword:
					return GetComponent<TwoHandedWeapon>();
				default:
					throw new System.Exception("");
			}
		}

		private float DistanceToCombatTarget() => Vector3.Distance(combatTarget.transform.position, transform.position);

		/// <note>
		/// Unity hook
		/// <note>
		private void OnDrawGizmosSelected() {
			if (weapon) {
				Gizmos.color = Color.blue;
				Gizmos.DrawWireSphere(transform.position, weapon.Range);
			}
		}
	}
}