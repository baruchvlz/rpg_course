﻿using UnityEngine;

namespace RPG.Combat {
	[RequireComponent(typeof(Health))]
	[RequireComponent(typeof(Animator))]
	public class CombatTarget : MonoBehaviour {
		private Health health;
		private Animator animator;

		public Health HitPoints { get { return health; } }

		void Awake() {
			health = GetComponent<Health>();
			animator = GetComponent<Animator>();
		}

		public void TakeDamage(float amount) {
			health.Decrease(amount);

			if (health.IsDead) {
				SummonDeath();
			}
		}

		private void SummonDeath() {
			animator.SetTrigger("Dead");
		}
	}
}