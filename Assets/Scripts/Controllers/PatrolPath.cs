﻿using UnityEngine;

namespace RPG.Control {
	public class PatrolPath : MonoBehaviour {
		private int currentPosition = 0;

		public int GetNextPositionIndex() {
			if (currentPosition == transform.childCount - 1) {
				currentPosition = 0;
			} else { 
				currentPosition += 1;
			}

			return currentPosition;
		}

		public Vector3 GetWaypointPosition(int index) => transform.GetChild(index).position;
		public Vector3 GetNextWaypointPosition(int index) => transform.GetChild(index == transform.childCount - 1 ? 0 : index + 1).position;

		private void OnDrawGizmos() {
			const float waypointGizmoRadius = 0.3f;

			for (int i = 0; i < transform.childCount; i++) {
				Gizmos.color = Color.red;
				Gizmos.DrawSphere(GetWaypointPosition(i), waypointGizmoRadius);
				Gizmos.DrawLine(GetWaypointPosition(i), GetNextWaypointPosition(i));
			}
		}
	}
}