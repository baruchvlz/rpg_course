using System;

using RPG.Control.Shared;
using RPG.Core.Events;
using RPG.Core.Interfaces;

using UnityEngine;
using UnityEngine.EventSystems;
// aggro, movement, attack
namespace RPG.Control {
	public enum EnemyType { Guard, Patrol }

	public class Enemy : CombatCharacter {
		[SerializeField] float chaseDistance = 5;
		[SerializeField] EnemyType type = EnemyType.Guard;
		[SerializeField] PatrolPath patrolPath;

		private Vector3 playerPosition;
		private Vector3 initialPosition;
		private Quaternion initialRotation;
		private GameObject playerGameObject;
		private float timeLastSeenPlayer = 0f;
		private float timeAtWaypoint = 0f;
		private Vector3 lastKnownPosition = Vector3.zero;
		private CombatStatus combatStatus = CombatStatus.InActive;

		private enum CombatStatus { Active, InActive }

		void Start() {
			Vector3 position = transform.position;
			Quaternion rotation = transform.rotation;

			// throw error if EnemyType.Patrol does not have a PatrolPath attached
			if (type == EnemyType.Patrol && patrolPath == null) {
				throw new MissingFieldException("Must have a patrol path attached for Patrol type enemies.");
			}
	
			if (type == EnemyType.Patrol) { transform.position = patrolPath.GetWaypointPosition(0); }

			initialPosition = new Vector3(position.x, position.y, position.z);
			initialRotation = new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);

		}

		void Update() {
			if (health.IsDead) {
				// trigger OnCombatEnd event to cancel after death
				if (combatStatus == CombatStatus.Active) {
					ChangeCombatStatusTo(CombatStatus.InActive);
				}

				return;
			}

			playerGameObject = GameObject.FindWithTag("Player");
			playerPosition = playerGameObject.transform.position;
			ManageHealth();

			// in attack range
			if (PlayerIsWithinChaseDistance(1)) { DoAttackAction(); }

			// out of attack radius
			if (!PlayerIsWithinChaseDistance(1)) { DoSuspicionAction(); }

			if (type == EnemyType.Patrol) { DoPatrolMovement(); }

			timeLastSeenPlayer += Time.deltaTime;
		}

		public override void ManageHealth() {
			if (PlayerIsWithinViewDistance()) { health.ShowHealthBar(TransitionSpeed.Fast); }

			if (PlayerIsWithinChaseDistance(1)) {
				health.Regenerate(TransitionSpeed.None);
			} else {
				TransitionSpeed transitionSpeed = TransitionSpeed.Normal;
				health.HideHealthBar(transitionSpeed);

				if (PlayerIsWithinChaseDistance(2)) {
					transitionSpeed = TransitionSpeed.Fast;
				}

				health.Regenerate(transitionSpeed);
			}
		}

		private void DoAttackAction() {
			// trigger OnCombatStart event
			if (combatStatus == CombatStatus.InActive) { ChangeCombatStatusTo(CombatStatus.Active); }

			combatCharacterRoleActions.Attack(playerGameObject);
			lastKnownPosition = new Vector3(playerPosition.x, playerPosition.y, playerPosition.z);
			timeLastSeenPlayer = 0f;
		}

		private void DoSuspicionAction() {
			if (lastKnownPosition == Vector3.zero) { return; }

			mover.StartMoveAction(lastKnownPosition);
			ChangeCombatStatusTo(CombatStatus.InActive);

			// go back to original position
			if (timeLastSeenPlayer > 3f) {
				// only reset to original position if enemy is a guard
				if (type == EnemyType.Guard) { mover.StartMoveAction(initialPosition); }
				// reset last known position
				if (lastKnownPosition != Vector3.zero) { lastKnownPosition = Vector3.zero; }
			}
		}

		private void DoPatrolMovement() {
			if (mover.IsAtDestination()) {
				print("At waypoint");
				timeAtWaypoint += Time.deltaTime;

				if (timeAtWaypoint > 3f) {
					print("Moving to next way point");
					mover.MoveTo(patrolPath.GetWaypointPosition(patrolPath.GetNextPositionIndex()));
					timeAtWaypoint = 0f;
				}
			}
		}

		private float GetDistanceToPlayerObject() => Vector3.Distance(transform.position, playerPosition);

		private bool PlayerIsWithinViewDistance() => GetDistanceToPlayerObject() < 20;
		private bool PlayerIsWithinChaseDistance(int multiplier) => GetDistanceToPlayerObject() < (chaseDistance * multiplier);

		private void ChangeCombatStatusTo(CombatStatus newCombatStatus) {
			if (combatStatus == newCombatStatus) { return; }
			if (combatStatus == CombatStatus.Active) { scheduler.CancelCurrentAction(); }

			combatStatus = newCombatStatus;

			ExecuteEvents.Execute<ICombatEvent>(
				playerGameObject,
				null,
				(playerInstance, _) => {
					if (combatStatus == CombatStatus.Active) {
						playerInstance.OnCombatStart(gameObject);
					} else {
						playerInstance.OnCombatEnd(gameObject);
					}
				}
			);
		}

		/// <note>
		/// Unity hook
		/// <note>
		private void OnDrawGizmosSelected() {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(transform.position, chaseDistance);

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transform.position, 20);
		}
	}
}
