﻿using System.Collections.Generic;

using RPG.Combat;
using RPG.Control.Shared;
using RPG.Core.Events;
using RPG.Core.Interfaces;
using RPG.Core.Utils;

using UnityEngine;

namespace RPG.Control {
	public class Player : CombatCharacter, ICombatEvent {
		private List<GameObject> enemyGameObjectList = new List<GameObject>();
		private GameObject focusedEnemy;

		void Update() {
			if (health.IsDead) { return; }
			ManageHealth();

			// Mouse left click is the trigger for player interactions
			if (Input.GetMouseButton(0)) {
				// Check for combat action
				if (MouseActionStartCombat()) { return; }
				// Move to point
				if (MouseActionMove()) { return; }
			}
		}

		// ICombatEvent listeners
		public void OnCombatStart(GameObject enemyGameObject) {
			if (!enemyGameObjectList.Exists(enemy => enemy.GetInstanceID() == enemyGameObject.GetInstanceID())) {
				enemyGameObjectList.Add(enemyGameObject);

				if (focusedEnemy != null && enemyGameObject.GetInstanceID() == focusedEnemy.GetInstanceID()) {
					combatCharacterRoleActions.Attack(enemyGameObject);
				}
			}
		}

		public void OnCombatEnd(GameObject enemyGameObject) {
			if (enemyGameObjectList.Exists(enemy => enemy.GetInstanceID() == enemyGameObject.GetInstanceID())) {
				enemyGameObjectList.Remove(enemyGameObject);
			}
		}
		//

		public override void ManageHealth() {
			health.ShowHealthBar(TransitionSpeed.Instant);

			if (enemyGameObjectList.Count != 0) {
				health.Regenerate(TransitionSpeed.VerySlow);
			} else {
				health.Regenerate(TransitionSpeed.Fast);
			}
		}

		private bool MouseActionStartCombat() {
			Ray mouseRay = CameraUtils.GetMouseRay();
			RaycastHit[] hits = Physics.RaycastAll(mouseRay);

			foreach (RaycastHit hit in hits) {
				if (hit.transform.tag != "Enemy") { continue; }
				CombatTarget target = hit.transform.GetComponent<CombatTarget>();

				if (!combatCharacterRoleActions.CanAttack(target.gameObject)) { continue; }
				focusedEnemy = target.gameObject;
				combatCharacterRoleActions.Attack(target.gameObject);
				return true;
			}

			return false;
		}

		private bool MouseActionMove() {
			Ray mouseRay = CameraUtils.GetMouseRay();
			bool hasHit = Physics.Raycast(mouseRay, out RaycastHit hit);

			if (hasHit) {
				mover.StartMoveAction(hit.point);
				return true;
			}

			return false;
		}
	}
}