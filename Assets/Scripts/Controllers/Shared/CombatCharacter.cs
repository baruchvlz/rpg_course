using RPG.Combat;
using RPG.Combat.Interfaces;
using RPG.Combat.Role;
using RPG.Combat.Shared;
using RPG.Core;
using RPG.Core.Interfaces;
using RPG.Movement;

using UnityEngine;

namespace RPG.Control.Shared {
	///<summary>
	/// CombatCharacter is the base controller for RPG.Control.Player and RPG.Control.Enemy
	/// contains shared properties and their initialization
	///<summary>
	[RequireComponent(typeof(Mover))]
	[RequireComponent(typeof(Health))]
	[RequireComponent(typeof(Scheduler))]
	[RequireComponent(typeof(CombatCharacterRole))]
	public abstract class CombatCharacter : MonoBehaviour {
		[SerializeField] CombatCharacterRoleType roleType = CombatCharacterRoleType.Fighter;

		internal Mover mover;
		internal Health health;
		internal Scheduler scheduler;
		internal ICombatCharacterRoleActions combatCharacterRoleActions;

		public abstract void ManageHealth();

		void Awake() {
			mover = GetComponent<Mover>();
			health = GetComponent<Health>();
			scheduler = GetComponent<Scheduler>();

			// character class (fighter, mage, etc)
			combatCharacterRoleActions = GetCombatCharacterRole();
		}

		internal ICombatCharacterRoleActions GetCombatCharacterRole() {
			switch (roleType) {
				case CombatCharacterRoleType.Fighter:
					return GetComponent<Fighter>();
				case CombatCharacterRoleType.Mage:
				default:
					throw new System.NotImplementedException("Class not yet implemented");
			}
		}
	}
}