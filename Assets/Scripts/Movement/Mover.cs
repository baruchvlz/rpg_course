﻿using RPG.Core;
using RPG.Core.Interfaces;

using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement {
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Scheduler))]
	[RequireComponent(typeof(Animator))]
	public class Mover : MonoBehaviour, IAction {
		private NavMeshAgent navMeshAgent;
		private Scheduler scheduler;
		private Animator animator;

		public bool IsAtDestination() => navMeshAgent.remainingDistance < 1;
		public Vector3 Destination() => new Vector3(navMeshAgent.destination.x, navMeshAgent.destination.y, navMeshAgent.destination.z);

		private void Awake() {
			navMeshAgent = GetComponent<NavMeshAgent>();
			scheduler = GetComponent<Scheduler>();
			animator = GetComponent<Animator>();
		}

		void Update() {
			UpdateAnimator();
		}

		public void StartMoveAction(Vector3 destination) {
			scheduler.StartAction(this);
			MoveTo(destination);
		}

		public float MoveTo(Vector3 destination) {
			navMeshAgent.destination = destination;
			navMeshAgent.isStopped = false;

			return navMeshAgent.remainingDistance;
		}

		public void Cancel() {
			navMeshAgent.isStopped = true;
		}

		private void UpdateAnimator() {
			Vector3 velocity = navMeshAgent.velocity;
			Vector3 localVelocity = transform.InverseTransformDirection(velocity);

			animator.SetFloat("ForwardAnimation", localVelocity.z);
		}
	}
}