namespace RPG.Core.Interfaces {
	public interface IAction {
		void Cancel();
	}
}