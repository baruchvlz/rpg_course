using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Core.Events {
	public interface ICombatEvent : IEventSystemHandler {
		void OnCombatStart(GameObject gameObject);
		void OnCombatEnd(GameObject gameObject);
	}
}