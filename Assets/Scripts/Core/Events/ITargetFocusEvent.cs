/// <note>
/// This is used for Tab targetting. This is currently not implemented anywhere.null 22.03.2020
/// <note>
using UnityEngine.EventSystems;

namespace RPG.Core.Events {
	/// <summary>
	/// Events emitted when a target is focused or unfocused
	/// <summary>
	public interface ITargetFocusEvent : IEventSystemHandler {
		void OnTargetFocus();
		void OnTargetUnfocus();
	}
}