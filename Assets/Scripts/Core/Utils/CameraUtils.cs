using UnityEngine;

namespace RPG.Core.Utils {
	public class CameraUtils : MonoBehaviour {
		public static Ray GetMouseRay() {
			return Camera.main.ScreenPointToRay(Input.mousePosition);
		}
	}
}