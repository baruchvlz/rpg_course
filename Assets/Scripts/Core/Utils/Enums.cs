namespace RPG.Core.Interfaces {
	public enum CombatCharacterRoleType {
		Fighter,
		Mage,
	}

	public enum TransitionSpeed {
		None = 0,
		Instant = 0,
		VerySlow = 1,
		Slow = 2,
		Normal = 3,
		Fast = 4,
		VeryFast = 5
	}
}