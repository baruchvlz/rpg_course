namespace RPG.Core.Utils {
	public class MathUtils {
		public static float Percentage(float numerator, float denominator) {
			return (numerator / denominator) * 100;
		}
	}
}