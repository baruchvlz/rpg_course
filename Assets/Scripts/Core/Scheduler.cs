using RPG.Core.Interfaces;

using UnityEngine;

namespace RPG.Core {
	public class Scheduler : MonoBehaviour {
		private IAction currentAction;

		public void StartAction(IAction action) {
			if (currentAction != action) {
				CancelCurrentAction();
			}

			currentAction = action;
		}

		public void CancelCurrentAction() {
			if (currentAction != null) {
				currentAction.Cancel();
			}
		}
	}
}